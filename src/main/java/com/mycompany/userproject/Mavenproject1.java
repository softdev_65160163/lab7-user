/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.userproject;

import java.util.ArrayList;

/**
 *
 * @author bbnpo
 */
public class Mavenproject1 {

    public static void main(String[] args) {
        user admin = new user("admin", "Administrator", "pass1234", 'm', 'a');
        user user1 = new user("user1", "User1", "pass1234", 'm', 'u');
        user user2 = new user("user2", "User2", "pass1234", 'f', 'u');

        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        user[] userArr = new user[3];
        System.out.println("Arr");
        userArr[0] = admin;
        userArr[1] = user1;
        userArr[2] = user2;
        for (int i = 0; i < userArr.length; i++) {
            System.out.println(userArr[i]);

        }
        System.out.println("List");
        ArrayList<user> userList = new ArrayList<user>();
        userList.add(admin);
        System.out.println(userList.get(0)+"list size"+userList.size());
        userList.add(user1);
        System.out.println(userList.get(1)+"list size"+userList.size());
        userList.add(user2);
        System.out.println(userList.get(2)+"list size"+userList.size());
        System.out.println("loop");
        for (int i=0;i<userList.size();i++){
            System.out.println(userList.get(i));
            
        }
    }
}
