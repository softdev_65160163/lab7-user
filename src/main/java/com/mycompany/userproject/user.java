/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.userproject;

import java.io.Serializable;

/**
 *
 * @author bbnpo
 */
public class user implements Serializable{

    private int id;
    private String login;
    private String name;
    private String password;
    private char gender;//m,f
    private char role;//a,u
    private static int lastId = 1;

    public user(String login, String name, String password, char gender, char role) {
      this(user.lastId++,login,name , password,gender,role);
          
    }
    
    

    public user(int id, String login, String name, String password, char gender, char role) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public char getRole() {
        return role;
    }

    public void setRole(char role) {
        this.role = role;
    }

    public static int getLastId() {
        return lastId;
    }

    public static void setLastId(int lastId) {
        user.lastId = lastId;
    }

    @Override
    public String toString() {
        return "user{" + "id=" + id + ", login=" + login + ", name=" + name + ", password=" + password + ", gender=" + gender + ", role=" + role + '}';
    }

}
